/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.areacalculate;

/**
 *
 * @author nymr3kt
 */
public class Triangle {
    private double h ;
    private double w ;
    public static double divide = 1.0/2 ;
    
    public Triangle(double h , double w){
        this.h = h;
        this.w = w;
    }
    
    public double calArea( ){
        return divide * h * w ;
        
    }
    
    public void setHandW(double h , double w){
        if(h <= 0 && w <= 0){
            System.out.println("Error: Hight and Wide must more than zero!!");
            return ;
        }
        this.h = h;
        this.w = w;
    }
    public double getH(double h){
        return h;
        
    }
    public double getW(double w){
        return w;
        
    }
    
}
