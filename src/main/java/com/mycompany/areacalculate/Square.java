/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.areacalculate;

/**
 *
 * @author nymr3kt
 */
public class Square {
    private double w ;
    
    public Square(double w){
        this.w = w;
        
    }
    public double CalArea(){
        return w * w ;
        
    }
    public void setW(double w){
        if(w <= 0){
            System.out.println("Error: Wide must more than Zero!!");
            return ;
        }
        this.w = w;
    }
    public double getW(double w){
        return w ;
    }
}

